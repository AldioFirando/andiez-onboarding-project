package com.andiez.onboarding.helper.permission

interface ResponsePermissionCallback {
     fun onResult(permissionResult: List<String>)
}