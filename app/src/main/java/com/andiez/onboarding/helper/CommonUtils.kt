package com.andiez.onboarding.helper

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager.NameNotFoundException
import android.net.Uri
import android.os.Bundle
import com.jakewharton.processphoenix.ProcessPhoenix
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.andiez.onboarding.BuildConfig
import com.andiez.onboarding.data.local.RealmHelper
import com.andiez.onboarding.data.local.prefs.DataConstant
import com.andiez.onboarding.data.local.prefs.SuitPreferences
import com.andiez.onboarding.data.model.User
import com.andiez.onboarding.feature.login.LoginActivity
import com.andiez.onboarding.feature.splashscreen.SplashScreenActivity
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by dodydmw19 on 7/18/18.
 */

class CommonUtils {

    companion object {

        fun openAppInStore(context: Context) {
            // you can also use BuildConfig.APPLICATION_ID
            try {
                val appId = context.packageName
                val rateIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$appId")
                )
                var marketFound = false
                // find all applications able to handle our rateIntent
                val otherApps = context.packageManager
                    .queryIntentActivities(rateIntent, 0)
                for (otherApp in otherApps) {
                    // look for Google Play application
                    if (otherApp.activityInfo.applicationInfo.packageName == "com.android.vending") {

                        val otherAppActivity = otherApp.activityInfo
                        val componentName = ComponentName(
                            otherAppActivity.applicationInfo.packageName,
                            otherAppActivity.name
                        )
                        // make sure it does NOT open in the stack of your activity
                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        // task reparenting if needed
                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
                        // if the Google Play was already open in a search result
                        //  this make sure it still go to the app page you requested
                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        // this make sure only the Google Play app is allowed to
                        // intercept the intent
                        rateIntent.component = componentName
                        context.startActivity(rateIntent)
                        marketFound = true
                        break

                    }
                }

                // if GP not present on device, open web browser
                if (!marketFound) {
                    val webIntent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appId")
                    )
                    context.startActivity(webIntent)

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun restartApp(activity: Activity?) {
            activity?.let {
                activity.run {
                    val intent = Intent(activity, SplashScreenActivity::class.java)
                    ProcessPhoenix.triggerRebirth(activity, intent)
                }
            }
        }

        fun restartApp(context: Context?) {
            context?.let {
                context.run {
                    val intent = Intent(context, SplashScreenActivity::class.java)
                    ProcessPhoenix.triggerRebirth(context, intent)
                }
            }
        }

        fun setCamera(lat: Double, lng: Double, mapBox: MapboxMap?) {
            val position = CameraPosition.Builder()
                .target(LatLng(lat, lng))
                .zoom(15.0)
                .build()

            mapBox?.animateCamera(
                CameraUpdateFactory
                    .newCameraPosition(position), 3000
            )
        }

        fun clearLocalStorage() {
            val suitPreferences = SuitPreferences.instance()
            val currentUrl: String? = SuitPreferences.instance()?.getString(DataConstant.BASE_URL)
            suitPreferences?.clearSession()
            currentUrl?.let {
                SuitPreferences.instance()?.saveString(DataConstant.BASE_URL, currentUrl.toString())
            }
            val realm: RealmHelper<User> = RealmHelper()
            realm.removeAllData()
        }

        fun setDefaultBaseUrlIfNeeded() {
            val currentUrl: String? = SuitPreferences.instance()?.getString(DataConstant.BASE_URL)
            if (currentUrl == null) {
                SuitPreferences.instance()?.saveString(DataConstant.BASE_URL, BuildConfig.BASE_URL)
            }
        }

        fun createIntent(context: Context, actDestination: Class<out Activity>): Intent {
            return Intent(context, actDestination)
        }

        fun gotoLoginScreen(activity: Activity?) {
            activity?.let {
                val intent = Intent(it, LoginActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                it.startActivity(intent)
                it.finish()
            }
        }

        fun loadJSONFromAsset(json_name: String, context: Context): String? {
            val json: String
            try {
                val `is` = context.assets.open(json_name)
                val size = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                json = String(buffer, charset("UTF-8"))
            } catch (ex: IOException) {
                ex.printStackTrace()
                return null
            }

            return json
        }

        fun convertData(type: String?): CommonConstant.UpdateMode {
            return when (type) {
                "flexible" -> CommonConstant.UpdateMode.FLEXIBLE
                "immediate" -> CommonConstant.UpdateMode.IMMEDIATE
                else -> CommonConstant.UpdateMode.FLEXIBLE
            }
        }

        fun isUpdateAvailable(version: Int?): Boolean {
            version?.let {
                val currentVersion = BuildConfig.VERSION_CODE
                return version != 0 && currentVersion < it
            } ?: run {
                return false
            }
        }

        fun isPalindrome(words: String): Boolean {
            return words.replace(" ", "").lowercase() == words.replace(" ", "").reversed()
                .lowercase()
        }

        fun isPrime(number: Int): Boolean {
            if (number <= 3) return number > 1
            if (number % 2 == 0 || number % 3 == 0) return false
            var i = 5
            while (i * i <= number) {
                if (number % i == 0 || number % (i + 2) == 0) return false
                i += 6
            }
            return true
        }

        fun convertFormatDate(oldDate: String, format: String = "yyyy-MM-dd"): String {
            val newFormat = "d MMMM, yyyy"
            val sdf = SimpleDateFormat(format, Locale.getDefault())
            val date = sdf.parse(oldDate)
            sdf.applyPattern(newFormat)
            return sdf.format(date!!)
        }
    }


}