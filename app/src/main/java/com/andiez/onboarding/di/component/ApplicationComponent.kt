package com.andiez.onboarding.di.component

import com.andiez.onboarding.di.module.ApplicationModule
import com.andiez.onboarding.di.scope.SuitCoreApplicationScope
import com.andiez.onboarding.feature.choosebutton.ChooseButtonPresenter
import com.andiez.onboarding.feature.event.EventPresenter
import com.andiez.onboarding.feature.event.search.SearchPlacePresenter
import com.andiez.onboarding.feature.guests.GuestPresenter
import com.andiez.onboarding.feature.home.HomePresenter
import com.andiez.onboarding.feature.login.LoginPresenter
import com.andiez.onboarding.feature.member.MemberPresenter
import com.andiez.onboarding.feature.sidemenu.SideMenuPresenter
import com.andiez.onboarding.feature.splashscreen.SplashScreenPresenter
import com.andiez.onboarding.firebase.remoteconfig.RemoteConfigPresenter
import com.andiez.onboarding.onesignal.OneSignalPresenter
import dagger.Component

@SuitCoreApplicationScope
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(memberPresenter: MemberPresenter)

    fun inject(loginPresenter: LoginPresenter)

    fun inject(splashScreenPresenter: SplashScreenPresenter)

    fun inject(oneSignalPresenter: OneSignalPresenter)

    fun inject(remoteConfigPresenter: RemoteConfigPresenter)

    fun inject(searchPlacePresenter: SearchPlacePresenter)

    fun inject(eventPresenter: EventPresenter)

    fun inject(sideMenuPresenter: SideMenuPresenter)

    fun inject(homePresenter: HomePresenter)

    fun inject(chooseButtonPresenter: ChooseButtonPresenter)

    fun inject(guestPresenter: GuestPresenter)
}