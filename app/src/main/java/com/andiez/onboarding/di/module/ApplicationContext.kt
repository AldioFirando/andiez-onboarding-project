package com.andiez.onboarding.di.module

import javax.inject.Qualifier

@Qualifier @Retention annotation class ApplicationContext
