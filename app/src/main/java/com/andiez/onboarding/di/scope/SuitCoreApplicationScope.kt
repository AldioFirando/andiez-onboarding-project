package com.andiez.onboarding.di.scope

import javax.inject.Scope

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class SuitCoreApplicationScope