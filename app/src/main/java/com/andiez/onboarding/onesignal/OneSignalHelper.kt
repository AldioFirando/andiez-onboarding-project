package com.andiez.onboarding.onesignal

import android.content.Context
import com.onesignal.OSNotificationOpenedResult
import com.onesignal.OSNotificationReceivedEvent
import com.onesignal.OneSignal
import com.andiez.onboarding.BuildConfig
import com.andiez.onboarding.data.local.prefs.DataConstant
import com.andiez.onboarding.data.local.prefs.SuitPreferences
import com.andiez.onboarding.helper.CommonConstant
import org.json.JSONObject
import timber.log.Timber


/**
 * Created by @dodydmw19 on 05, October, 2021
 */

class OneSignalHelper {

    companion object {
        fun initOneSignal(context: Context) {
            // OneSignal Initialization
            OneSignal.initWithContext(context)
            OneSignal.setAppId(CommonConstant.ONE_SIGNAL_APP_ID)

            initNotificationOpenedHandler()
            notificationForeGroundHandler()

            OneSignal.unsubscribeWhenNotificationsAreDisabled(true)
            OneSignal.pauseInAppMessages(true)
            OneSignal.setLocationShared(false)

            val device = OneSignal.getDeviceState()
            val userId = device?.userId //push player_id

            userId?.let {
                SuitPreferences.instance()?.saveString(DataConstant.PLAYER_ID, it)
            }

            if (BuildConfig.DEBUG) {
                OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)
            }
        }

        private fun initNotificationOpenedHandler() {
            OneSignal.setNotificationOpenedHandler { result: OSNotificationOpenedResult ->
                OneSignal.onesignalLog(
                    OneSignal.LOG_LEVEL.VERBOSE,
                    "OSNotificationOpenedResult result: $result"
                )
            }
        }

        private fun notificationForeGroundHandler() {
            OneSignal.setNotificationWillShowInForegroundHandler { notificationReceivedEvent: OSNotificationReceivedEvent ->
                OneSignal.onesignalLog(
                    OneSignal.LOG_LEVEL.VERBOSE, "NotificationWillShowInForegroundHandler fired!" +
                            " with notification event: " + notificationReceivedEvent.toString()
                )
                val notification = notificationReceivedEvent.notification
                //val data = notification.additionalData
                notificationReceivedEvent.complete(notification)
            }
        }

        fun sendNotification(event: String?, guest: String?) {
            try {
                var message = ""
                OneSignal.postNotification(
                    JSONObject(
                        "{'contents': { " +
                                "'en': 'Anda ${
                                    when {
                                        guest != null && event != null -> "memilih $guest dan $event"
                                        guest != null -> "memilih $guest dan tidak memilih event."
                                        event != null -> "memilih $event dan tidak memilih guest."
                                        else -> "tidak memilih apapun."
                                    }
                                }'}," +
                                "'include_player_ids': ['" + SuitPreferences.instance()
                            ?.getString(DataConstant.PLAYER_ID) + "']}"
                    ), object : OneSignal.PostNotificationResponseHandler {
                        override fun onSuccess(response: JSONObject?) {
                            message += "Notifikasi berhasil dikirim."
                            Timber.tag("OneSignal").d(message)
                        }

                        override fun onFailure(response: JSONObject?) {
                            message += "Notifikasi gagal dikirim."
                            Timber.tag("OneSignal").e(message)
                        }
                    }
                )
            } catch (e: Exception) {
                Timber.tag("OneSignal").e(e.message.toString())
            }
        }
    }

}