package com.andiez.onboarding.onesignal

import com.andiez.onboarding.base.presenter.MvpView

/**
 * Created by dodydmw19 on 6/12/19.
 */

interface OneSignalView : MvpView {

    fun onRegisterIdSuccess(message: String?)

    fun onRegisterIdFailed(error: Any?)

}