package com.andiez.onboarding.data.remote.services

import com.andiez.onboarding.data.model.Guest
import com.andiez.onboarding.data.model.Place
import com.andiez.onboarding.data.model.User
import com.andiez.onboarding.data.remote.wrapper.MapBoxResults
import com.andiez.onboarding.data.remote.wrapper.Results
import io.reactivex.Flowable
import io.reactivex.Single
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

/**
 * Created by DODYDMW19 on 8/3/2017.
 */

interface APIService {

    @GET("users")
    fun getMembers(
            @Query("per_page") perPage: Int,
            @Query("page") page: Int): Single<Results<User>>

    @GET("users")
    fun getMembersCoroutinesAsync(
            @Query("per_page") perPage: Int,
            @Query("page") page: Int): Deferred<Results<User>>

    @GET
    fun searchPlaceAsync(@Url url: String?): Deferred<MapBoxResults<Place>>

    @GET("596dec7f0f000023032b8017")
    fun getGuest(): Deferred<List<Guest>>
}
