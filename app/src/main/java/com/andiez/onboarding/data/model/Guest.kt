package com.andiez.onboarding.data.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Guest(
    @PrimaryKey @field:SerializedName("id") var id: Int = 0,
    @field:SerializedName("name") var name: String = "",
    @field:SerializedName("birthdate") var birthdate: String = "",
    var image: String = "https://media-cdn.tripadvisor.com/media/photo-i/1a/9e/40/c0/alas-purwo-is-one-of.jpg"
) : RealmObject()