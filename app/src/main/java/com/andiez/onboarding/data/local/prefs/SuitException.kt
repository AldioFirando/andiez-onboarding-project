package com.andiez.onboarding.data.local.prefs

class SuitException(message: String?) : RuntimeException(message)