package com.andiez.onboarding.firebase.remoteconfig

import com.andiez.onboarding.base.presenter.MvpView
import com.andiez.onboarding.data.model.UpdateType
import com.andiez.onboarding.helper.CommonConstant

interface RemoteConfigView : MvpView {

    fun onUpdateBaseUrlNeeded(type: String?, url: String?)

    fun onUpdateTypeReceive(update: UpdateType?)

    fun onGreetingReceived(message: String, isAllCaps: Boolean)

    fun onGreetingIsLoading(message: String)

}