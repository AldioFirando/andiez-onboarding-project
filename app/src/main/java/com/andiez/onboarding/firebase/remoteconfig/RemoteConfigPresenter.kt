package com.andiez.onboarding.firebase.remoteconfig

import android.content.Context
import android.util.Log
import androidx.lifecycle.LifecycleOwner
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.gson.Gson
import com.andiez.onboarding.BaseApplication
import com.andiez.onboarding.BuildConfig
import com.andiez.onboarding.R
import com.andiez.onboarding.base.presenter.BasePresenter
import com.andiez.onboarding.data.local.prefs.DataConstant
import com.andiez.onboarding.data.local.prefs.SuitPreferences
import com.andiez.onboarding.data.model.UpdateType
import com.andiez.onboarding.helper.CommonConstant
import com.andiez.onboarding.helper.CommonUtils
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.get
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings

class RemoteConfigPresenter : BasePresenter<RemoteConfigView> {

    private lateinit var mFireBaseRemoteConfig: FirebaseRemoteConfig
    private var mvpView: RemoteConfigView? = null

    init {
        BaseApplication.applicationComponent.inject(this)
        setupFireBaseRemoteConfig()
    }

    private fun setupFireBaseRemoteConfig() {
        mFireBaseRemoteConfig = Firebase.remoteConfig
        val configSettings = remoteConfigSettings { minimumFetchIntervalInSeconds = 3600 }
        mFireBaseRemoteConfig.setConfigSettingsAsync(
            configSettings
        )
        mFireBaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
    }

    fun checkBaseUrl() {
        if (mFireBaseRemoteConfig == null) {
            setupFireBaseRemoteConfig()
        }

        mFireBaseRemoteConfig.fetchAndActivate()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val updated = task.result
                    Log.d("***", "Config params updated: $updated")
                }

                val newBaseUrl: String? =
                    mFireBaseRemoteConfig.getString(CommonConstant.NEW_BASE_URL)
                val currentUrl: String? =
                    SuitPreferences.instance()?.getString(DataConstant.BASE_URL)
                if (newBaseUrl != null) {
                    if (newBaseUrl.toString().isNotEmpty()) {
                        if (currentUrl != newBaseUrl) mvpView?.onUpdateBaseUrlNeeded(
                            "new",
                            newBaseUrl.toString()
                        )
                    } else {
                        if (currentUrl != null && currentUrl.isNotEmpty() && currentUrl != BuildConfig.BASE_URL) {
                            mvpView?.onUpdateBaseUrlNeeded("default", BuildConfig.BASE_URL)
                        } else {
                            CommonUtils.setDefaultBaseUrlIfNeeded()
                        }
                    }
                }
            }
    }

    fun getUpdateType(context: Context) {
        var updateFromConsole: String? = ""
        val updateDefaultJSON = CommonUtils.loadJSONFromAsset("update.json", context)
        val gSon = Gson()
        var updateType: UpdateType? = gSon.fromJson(updateDefaultJSON, UpdateType::class.java)

        mFireBaseRemoteConfig.fetchAndActivate()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val updated = task.result
                    Log.d("***", "Config params updated: $updated")
                }

                if (mFireBaseRemoteConfig.getString(CommonConstant.NOTIFY_UPDATE_TYPE) != null && mFireBaseRemoteConfig.getString(
                        CommonConstant.NOTIFY_UPDATE_TYPE
                    ).isNotEmpty()
                ) {
                    updateFromConsole =
                        mFireBaseRemoteConfig.getString(CommonConstant.NOTIFY_UPDATE_TYPE)
                    updateType = UpdateType()
                    updateType = gSon.fromJson(updateFromConsole.toString(), UpdateType::class.java)
                    mvpView?.onUpdateTypeReceive(updateType)
                } else {
                    mvpView?.onUpdateTypeReceive(updateType)
                }
            }
    }

    fun getGreetingMessage() {
        mFireBaseRemoteConfig.getString(CommonConstant.LOADING_PHRASE_CONFIG_KEY)
            .let { mvpView?.onGreetingIsLoading(it) }

        mFireBaseRemoteConfig.fetchAndActivate()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val updated = task.result
                    val message = mFireBaseRemoteConfig[CommonConstant.GREET_MESSAGE_KEY].asString()
                    val isCaps =
                        mFireBaseRemoteConfig[CommonConstant.GREET_MESSAGE_CAPS_KEY].asBoolean()
                    mvpView?.onGreetingReceived(message, isCaps)
                    Log.d("***", "Config params updated: $updated")
                } else {
                    mvpView?.onGreetingReceived("Welome, Gagal mendapatkan value", false)
                    Log.d("***", "Fetch failed")
                }
            }
    }

    override fun onDestroy() {
        detachView()
    }

    override fun attachView(view: RemoteConfigView) {
        mvpView = view
        // Initialize this presenter as a lifecycle-aware when a view is a lifecycle owner.
        if (mvpView is LifecycleOwner) {
            (mvpView as LifecycleOwner).lifecycle.addObserver(this)
        }
    }

    override fun detachView() {
        mvpView = null
    }

}