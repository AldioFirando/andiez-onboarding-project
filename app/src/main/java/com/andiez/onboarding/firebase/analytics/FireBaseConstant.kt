package com.andiez.onboarding.firebase.analytics


/**
 * Created by @dodydmw19 on 03, December, 2020
 */

object FireBaseConstant {

    // Screen View Tracking
    const val SCREEN_LOGIN = "Login"

    const val SCREEN_HOME = "Home"

    // Event Tracking
    const val EVENT_SKIP_LOGIN = "SkipLogin"

}