package com.andiez.onboarding

import android.app.Activity
import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.andiez.onboarding.onesignal.OneSignalHelper
import com.facebook.drawee.backends.pipeline.Fresco
import com.mapbox.mapboxsdk.Mapbox
import com.andiez.onboarding.data.local.prefs.SuitPreferences
import com.andiez.onboarding.di.component.ApplicationComponent
import com.andiez.onboarding.di.component.DaggerApplicationComponent
import com.andiez.onboarding.di.module.ApplicationModule
import com.andiez.onboarding.firebase.analytics.FireBaseHelper
import com.andiez.onboarding.helper.ActivityLifecycleCallbacks
import com.andiez.onboarding.helper.CommonConstant
import com.andiez.onboarding.helper.CommonUtils
import com.andiez.onboarding.helper.rxbus.RxBus
import io.realm.Realm
import io.realm.RealmConfiguration


/**
 * Created by DODYDMW19 on 1/30/2018.
 */

class BaseApplication : MultiDexApplication() {

    val mActivityLifecycleCallbacks = ActivityLifecycleCallbacks()

    init {
        instance = this
    }

    companion object {
        lateinit var applicationComponent: ApplicationComponent
        lateinit var appContext: Context
        lateinit var bus: RxBus
        private var instance: BaseApplication? = null

        fun currentActivity(): Activity? {
            return instance?.mActivityLifecycleCallbacks?.currentActivity
        }
    }

    override fun onCreate() {
        super.onCreate()

        registerActivityLifecycleCallbacks(mActivityLifecycleCallbacks)
        //bus = RxBus()

        // Initial Preferences
        SuitPreferences.init(applicationContext)
        FireBaseHelper.instance().initialize(this)

        CommonUtils.setDefaultBaseUrlIfNeeded()

        appContext = applicationContext
        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()

        Fresco.initialize(this)

        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
            .schemaVersion(1)
            .allowWritesOnUiThread(true)
            .deleteRealmIfMigrationNeeded()
            //.encryptionKey(CommonUtils.getKey()) // encrypt realm
            .build()
        Realm.setDefaultConfiguration(realmConfiguration)

        OneSignalHelper.initOneSignal(this)

        Mapbox.getInstance(this, CommonConstant.MAP_BOX_TOKEN)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}