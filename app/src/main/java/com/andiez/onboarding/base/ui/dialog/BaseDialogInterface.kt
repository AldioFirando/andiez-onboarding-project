package com.andiez.onboarding.base.ui.dialog


interface BaseDialogInterface {
    fun onSubmitClick()
    fun onDismissClick()
}