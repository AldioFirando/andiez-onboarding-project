package com.andiez.onboarding.base.ui.recyclerview

//Callback only provides load more for now
interface EndlessScrollCallback{
    fun loadMore()
}