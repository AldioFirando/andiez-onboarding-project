package com.andiez.onboarding.feature.guests

import com.andiez.onboarding.base.presenter.MvpView
import com.andiez.onboarding.data.model.Guest
import io.realm.RealmResults

interface GuestView: MvpView {

    fun onGuestCacheLoaded(guests: RealmResults<Guest>?)

    fun onGuestLoaded(guests: List<Guest>?)

    fun onGuestEmpty()

    fun onFailed(error: Any?)

}