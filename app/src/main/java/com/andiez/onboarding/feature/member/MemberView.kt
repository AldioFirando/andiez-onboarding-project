package com.andiez.onboarding.feature.member

import com.andiez.onboarding.base.presenter.MvpView
import com.andiez.onboarding.data.model.User
import io.realm.RealmList
import io.realm.RealmResults

/**
 * Created by DODYDMW19 on 1/30/2018.
 */

interface MemberView : MvpView {

    fun onMemberCacheLoaded(members: RealmResults<User>?)

    fun onMemberLoaded(members: List<User>?)

    fun onMemberEmpty()

    fun onFailed(error: Any?)

}