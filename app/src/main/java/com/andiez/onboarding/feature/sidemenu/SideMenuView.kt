package com.andiez.onboarding.feature.sidemenu

import com.andiez.onboarding.base.presenter.MvpView
import com.andiez.onboarding.data.model.SideMenu


/**
 * Created by dodydmw19 on 1/3/19.
 */

interface SideMenuView : MvpView {

    fun onSideMenuLoaded(sideMenus: List<SideMenu>?)

    fun onFailedLoadSideMenu(message: String?)

}