package com.andiez.onboarding.feature.event

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import com.andiez.onboarding.R
import com.andiez.onboarding.base.ui.BaseFragment
import com.andiez.onboarding.data.model.Event
import com.andiez.onboarding.databinding.ItemEventBinding
import com.andiez.onboarding.feature.choosebutton.ChooseButtonActivity
import com.andiez.onboarding.feature.events.EventActivity
import com.bumptech.glide.Glide


class ImageFragment : BaseFragment<ItemEventBinding>() {

    var data: Event? = null

    companion object {
        fun newInstance(data: Event): Fragment {
            val fragment = ImageFragment()
            fragment.data = data
            return fragment
        }
    }

    override fun getViewBinding(inflater: LayoutInflater, container: ViewGroup?): ItemEventBinding =
        ItemEventBinding.inflate(inflater, container, false)

    override fun onViewReady(savedInstanceState: Bundle?) {
        data?.let { event ->
            val uri = event.imageUrl.toUri().buildUpon().scheme("http").build()
            Glide.with(binding.imgItemEvent)
                .load(uri)
                .placeholder(R.drawable.loading_animation)
                .error(R.drawable.ic_broken_image)
                .into(binding.imgItemEvent)
            binding.tvItemEventName.text = data?.name
            binding.root.setOnClickListener {
                (requireActivity() as EventActivity).setResult(
                    ChooseButtonActivity.EVENT_REQUEST_CODE,
                    Intent().putExtra(ChooseButtonActivity.EVENT_CALLBACK_KEY, event.name)
                )
                (requireActivity() as EventActivity).sendAnalytics(event.id.toLong(), event.name)
                (requireActivity() as EventActivity).finish()
            }
        }
    }
}