package com.andiez.onboarding.feature.events

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.andiez.onboarding.R
import com.andiez.onboarding.base.ui.BaseActivity
import com.andiez.onboarding.databinding.ActivityEventBinding
import com.andiez.onboarding.feature.event.EventFragment
import com.andiez.onboarding.firebase.analytics.FireBaseHelper
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent

class EventActivity : BaseActivity<ActivityEventBinding>(), EventActivityView {

    private var fragmentPosition = 1
    private var eventFragment: Fragment = EventListFragment.newInstance()
    private lateinit var presenter: EventActivityPresenter

    override fun getViewBinding(): ActivityEventBinding =
        ActivityEventBinding.inflate(layoutInflater)

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupPresenter()
        setFragment()
        actionClick()
    }

    fun sendAnalytics(id: Long, name: String) {
        FireBaseHelper.instance().getFireBaseAnalytics()
            ?.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                param(FirebaseAnalytics.Param.ITEM_ID, id)
                param(FirebaseAnalytics.Param.ITEM_NAME, name)
                param(FirebaseAnalytics.Param.CONTENT_TYPE, "event")
            }
    }

    private fun setupPresenter() {
        presenter = EventActivityPresenter()
        presenter.attachView(this)
    }

    private fun actionClick() {
        with(binding) {
            btnAdd.setOnClickListener {
                setFragment()
            }
            toolbar.setNavigationOnClickListener { this@EventActivity.finish() }
        }
    }

    private fun setFragment() {
        when (fragmentPosition) {
            1 -> {
                fragmentPosition = 2
                eventFragment = EventListFragment.newInstance()
            }
            else -> {
                fragmentPosition = 1
                eventFragment = EventFragment.newInstance()
            }
        }
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(
            R.id.fragment_container,
            eventFragment
        )
        transaction.commit()
    }
}