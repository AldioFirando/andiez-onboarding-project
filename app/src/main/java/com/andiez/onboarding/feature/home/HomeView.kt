package com.andiez.onboarding.feature.home

import com.andiez.onboarding.base.presenter.MvpView

interface HomeView: MvpView {
    fun onLoginSuccess(message: String?)

    fun onLoginFailed(message: String?)

}