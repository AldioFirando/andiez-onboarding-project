package com.andiez.onboarding.feature.guests

import android.view.LayoutInflater
import android.view.ViewGroup
import com.andiez.onboarding.base.ui.adapter.BaseRecyclerAdapter
import com.andiez.onboarding.data.model.Guest
import com.andiez.onboarding.databinding.LayoutGridGuestBinding

class GuestAdapter : BaseRecyclerAdapter<Guest, GuestItemViewHolder>() {
    private lateinit var binding: LayoutGridGuestBinding
    private var onActionListener: GuestItemViewHolder.OnActionListener? = null

    fun setOnActionListener(onActionListener: GuestItemViewHolder.OnActionListener) {
        this.onActionListener = onActionListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GuestItemViewHolder {
        binding = LayoutGridGuestBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val view = GuestItemViewHolder(binding)
        onActionListener?.let { view.setOnActionListener(it) }
        return view
    }
}