package com.andiez.onboarding.feature.splashscreen

import android.annotation.SuppressLint
import com.andiez.onboarding.base.presenter.MvpView

/**
 * Created by dodydmw19 on 12/19/18.
 */

@SuppressLint("CustomSplashScreen")
interface SplashScreenView : MvpView {

    fun navigateToMainView()

}