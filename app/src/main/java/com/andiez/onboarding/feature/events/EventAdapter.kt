package com.andiez.onboarding.feature.events

import android.view.LayoutInflater
import android.view.ViewGroup
import com.andiez.onboarding.base.ui.adapter.BaseRecyclerAdapter
import com.andiez.onboarding.data.model.Event
import com.andiez.onboarding.databinding.LayoutListEventBinding

class EventAdapter : BaseRecyclerAdapter<Event, EventViewHolder>() {

    private lateinit var binding: LayoutListEventBinding
    private var onActionListener: EventViewHolder.OnActionListener? = null

    fun setOnActionListener(onActionListener: EventViewHolder.OnActionListener) {
        this.onActionListener = onActionListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        binding = LayoutListEventBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val view = EventViewHolder(binding)
        onActionListener?.let { view.setOnActionListener(it) }
        return view
    }
}