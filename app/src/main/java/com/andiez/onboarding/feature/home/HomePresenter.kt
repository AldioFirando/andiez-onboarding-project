package com.andiez.onboarding.feature.home

import com.andiez.onboarding.BaseApplication
import com.andiez.onboarding.base.presenter.BasePresenter

class HomePresenter : BasePresenter<HomeView> {
    private var mvpView: HomeView? = null

    init {
        BaseApplication.applicationComponent.inject(this)
    }
    fun login(){
        mvpView?.onLoginSuccess("success")
    }

    override fun onDestroy() {}

    override fun attachView(view: HomeView) {
        mvpView = view
    }

    override fun detachView() {
        mvpView = null
    }
}