package com.andiez.onboarding.feature.home

import android.content.Intent
import android.os.Bundle
import com.andiez.onboarding.R
import com.andiez.onboarding.base.ui.BaseActivity
import com.andiez.onboarding.databinding.ActivityHomeBinding
import com.andiez.onboarding.feature.choosebutton.ChooseButtonActivity
import com.andiez.onboarding.feature.login.LoginActivity
import com.andiez.onboarding.firebase.analytics.FireBaseConstant
import com.andiez.onboarding.firebase.analytics.FireBaseHelper
import com.andiez.onboarding.helper.CommonUtils
import com.andiez.onboarding.helper.socialauth.google.GoogleListener
import com.andiez.onboarding.helper.socialauth.google.GoogleSignInHelper
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent

class HomeActivity : BaseActivity<ActivityHomeBinding>(), HomeView, GoogleListener {

    private lateinit var homePresenter: HomePresenter
    private lateinit var mGoogleSignInHelper: GoogleSignInHelper

    private fun setupPresenter() {
        homePresenter = HomePresenter()
        homePresenter.attachView(this)
    }

    private fun sendAnalytics() {
        FireBaseHelper.instance().getFireBaseAnalytics()?.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, FireBaseConstant.SCREEN_HOME)
            param(FirebaseAnalytics.Param.SCREEN_CLASS, HomeActivity::class.java.simpleName)
        }
    }

    override fun getViewBinding(): ActivityHomeBinding = ActivityHomeBinding.inflate(layoutInflater)

    private fun setupSocialLogin() {
        // Google  initialization
        mGoogleSignInHelper =
            GoogleSignInHelper(this, getString(R.string.google_default_web_client_id), this)
    }

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupPresenter()
        setupSocialLogin()
        buttonAction()
        sendAnalytics()
    }

    override fun onLoginSuccess(message: String?) {
        showToast("Login Success")
        binding.tvWelcome.text = "Telah Login"
        binding.buttonLogin.text = getString(R.string.text_logout)
    }

    private fun signOut() {
        mGoogleSignInHelper.performSignOut()
        binding.tvWelcome.text = getString(R.string.text_welcome)
        binding.buttonLogin.text = getString(R.string.text_login)
    }

    private fun buttonAction() {
        with(binding) {
            buttonLogin.setOnClickListener {
                if (buttonLogin.text == getString(R.string.text_login)) {
                    mGoogleSignInHelper.performSignIn(this@HomeActivity)
                } else {
                    signOut()
                }
            }

            buttonNext.setOnClickListener {
                val name = tfName.editText?.text.toString().trim()
                if (name == "") {
                    showDialogAlert("Peringatan", "Input tidak boleh kosong.")
                    return@setOnClickListener
                }
                val data = Bundle()
                data.putString(NAME_KEY, name)
                val isPalindrome = CommonUtils.isPalindrome(name)
                showDialogConfirmation("isPalindrome?", if (isPalindrome) "Is Palindrome" else "Not Palindrome", {
                    goToActivity(ChooseButtonActivity::class.java, data, false, isFinish = false)
                })
            }
        }
    }

    override fun onLoginFailed(message: String?) {
        message?.let {
            showToast(message.toString())
        }
    }

    override fun onGoogleAuthSignIn(authToken: String?, userId: String?) {
        // send token & user_id to server
        homePresenter.login()
    }

    override fun onGoogleAuthSignInFailed(errorMessage: String?) {
        showToast(errorMessage.toString())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            mGoogleSignInHelper.onActivityResult(requestCode, resultCode, data)
        }
    }

    companion object {
        const val NAME_KEY = "name"
    }
}