package com.andiez.onboarding.feature.choosebutton

import com.andiez.onboarding.BaseApplication
import com.andiez.onboarding.base.presenter.BasePresenter

class ChooseButtonPresenter: BasePresenter<ChooseButtonView> {
    private var mvpView: ChooseButtonView? = null

    init {
        BaseApplication.applicationComponent.inject(this)
    }

    override fun onDestroy() {}

    override fun attachView(view: ChooseButtonView) {
        mvpView = view
    }

    override fun detachView() {
        mvpView = null
    }
}