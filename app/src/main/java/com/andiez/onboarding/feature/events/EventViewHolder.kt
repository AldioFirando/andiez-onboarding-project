package com.andiez.onboarding.feature.events

import androidx.core.net.toUri
import androidx.recyclerview.widget.LinearLayoutManager
import com.andiez.onboarding.R
import com.andiez.onboarding.base.ui.adapter.viewholder.BaseItemViewHolder
import com.andiez.onboarding.data.model.Event
import com.andiez.onboarding.databinding.LayoutListEventBinding
import com.andiez.onboarding.helper.CommonUtils
import com.bumptech.glide.Glide

class EventViewHolder(private val binding: LayoutListEventBinding) :
    BaseItemViewHolder<Event>(binding) {
    private var mActionListener: OnActionListener? = null
    private var event: Event? = null

    fun setOnActionListener(onActionListener: OnActionListener) {
        mActionListener = onActionListener
    }

    override fun bind(data: Event?) {
        data?.let { event ->
            this.event = event
            event.imageUrl.let {
                val uri = it.toUri().buildUpon().scheme("http").build()
                Glide.with(binding.imgEvent)
                    .load(uri)
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
                    .into(binding.imgEvent)
            }
            binding.tvDate.text = CommonUtils.convertFormatDate(event.date, format = "dd-MM-yyyy")
            binding.tvName.text = event.name
            binding.tvDesc.text = event.description

            binding.root.setOnClickListener { mActionListener?.onItemClick(event) }

            val adapter = TagAdapter()
            binding.rvTags.layoutManager =
                LinearLayoutManager(binding.rvTags.context, LinearLayoutManager.HORIZONTAL, false)
            adapter.submitList(data.tags)
            binding.rvTags.adapter = adapter
        }
    }

    interface OnActionListener {
        fun onItemClick(item: Event)
    }
}