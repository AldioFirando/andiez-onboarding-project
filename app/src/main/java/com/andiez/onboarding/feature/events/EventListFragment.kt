package com.andiez.onboarding.feature.events

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andiez.onboarding.R
import com.andiez.onboarding.base.ui.BaseFragment
import com.andiez.onboarding.base.ui.recyclerview.BaseRecyclerView
import com.andiez.onboarding.data.model.Event
import com.andiez.onboarding.databinding.FragmentEventListBinding
import com.andiez.onboarding.feature.choosebutton.ChooseButtonActivity

class EventListFragment :
    BaseFragment<FragmentEventListBinding>(), EventListView {
    private lateinit var presenter: EventListPresenter

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentEventListBinding {
        return FragmentEventListBinding.inflate(inflater, container, false)
    }

    private fun setupPresenter() {
        presenter = EventListPresenter()
        presenter.attachView(this)
        presenter.getEvents()
    }

    private fun setupList(events: List<Event>) {
        val adapter = EventAdapter()
        adapter.add(events)
        adapter.setOnActionListener(object : EventViewHolder.OnActionListener {
            override fun onItemClick(item: Event) {
                (requireActivity() as EventActivity).setResult(
                    ChooseButtonActivity.EVENT_REQUEST_CODE,
                    Intent().putExtra(ChooseButtonActivity.EVENT_CALLBACK_KEY, item.name)
                )
                (requireActivity() as EventActivity).sendAnalytics(item.id.toLong(), item.name)
                (requireActivity() as EventActivity).finish()
            }
        })
        binding.rvEvents.apply {
            setUpAsList()
            setAdapter(adapter)
        }
        binding.rvEvents.stopShimmer()
        binding.rvEvents.showRecycler()
    }

    private fun setupEmptyView() {
        binding.rvEvents.setImageEmptyView(R.drawable.empty_state)
        binding.rvEvents.setTitleEmptyView(getString(R.string.txt_empty_member))
        binding.rvEvents.setContentEmptyView(getString(R.string.txt_empty_member_content))
        binding.rvEvents.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {
            override fun onClick(v: View?) {
                presenter.getEvents()
            }
        })
    }

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupPresenter()
        setupEmptyView()
    }

    override fun onEventsLoaded(data: List<Event>) {
        setupList(data)
    }

    companion object {
        fun newInstance(): EventListFragment =
            EventListFragment()
    }

}