package com.andiez.onboarding.feature.events

import com.andiez.onboarding.base.presenter.MvpView
import com.andiez.onboarding.data.model.Event

interface EventListView: MvpView {
    fun onEventsLoaded(data: List<Event>)
}