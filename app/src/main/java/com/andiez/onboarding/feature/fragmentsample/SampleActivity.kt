package com.andiez.onboarding.feature.fragmentsample

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.andiez.onboarding.R
import com.andiez.onboarding.base.ui.BaseActivity
import com.andiez.onboarding.databinding.ActivityTestBinding

/**
 * Created by dodydmw19 on 7/30/18.
 */

class SampleActivity : BaseActivity<ActivityTestBinding>(){

    private lateinit var mCurrentFragment: Fragment

    override fun getViewBinding(): ActivityTestBinding = ActivityTestBinding.inflate(layoutInflater)

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupToolbar(binding.mToolbar, true)
        setContentFragment(SampleFragment.newInstance())
    }

    private fun setContentFragment(fragment: Fragment) {
        mCurrentFragment = fragment
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, mCurrentFragment)
                .commitAllowingStateLoss()
    }

}