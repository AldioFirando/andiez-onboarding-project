package com.andiez.onboarding.feature.choosebutton

import android.content.Intent
import android.os.Bundle
import com.andiez.onboarding.R
import com.andiez.onboarding.base.ui.BaseActivity
import com.andiez.onboarding.data.model.UpdateType
import com.andiez.onboarding.databinding.ActivityChooseButtonBinding
import com.andiez.onboarding.feature.events.EventActivity
import com.andiez.onboarding.feature.guests.GuestActivity
import com.andiez.onboarding.feature.home.HomeActivity
import com.andiez.onboarding.firebase.remoteconfig.RemoteConfigPresenter
import com.andiez.onboarding.firebase.remoteconfig.RemoteConfigView
import com.andiez.onboarding.onesignal.OneSignalHelper

class ChooseButtonActivity : BaseActivity<ActivityChooseButtonBinding>(), ChooseButtonView,
    RemoteConfigView {
    private var selectedName = ""
    private var selectedEvent: String? = null
    private var selectedGuest: String? = null
    private lateinit var remoteConfigPresenter: RemoteConfigPresenter
    private lateinit var presenter: ChooseButtonPresenter

    private fun getData() {
        selectedName = intent.extras?.getString(HomeActivity.NAME_KEY).toString()
        binding.tvName.text = selectedName
    }

    override fun getViewBinding(): ActivityChooseButtonBinding =
        ActivityChooseButtonBinding.inflate(layoutInflater)

    override fun onViewReady(savedInstanceState: Bundle?) {
        actionClick()
        setupPresenter()
        getData()
    }

    private fun setupPresenter() {
        presenter = ChooseButtonPresenter()
        remoteConfigPresenter = RemoteConfigPresenter()
        presenter.attachView(this)
        remoteConfigPresenter.attachView(this)
    }

    private fun actionClick() {
        with(binding) {
            btnEvent.text = getString(R.string.text_choose_event)
            btnGuest.text = getString(R.string.text_choose_guest)
            btnSendNotification.setOnClickListener {
                OneSignalHelper.sendNotification(selectedEvent, selectedGuest)
            }
            btnEvent.setOnClickListener {
                goToActivity(EVENT_REQUEST_CODE, EventActivity::class.java, null)
            }
            btnGuest.setOnClickListener {
                goToActivity(GUEST_REQUEST_CODE, GuestActivity::class.java, null)
            }
            btnFetchConfig.setOnClickListener {
                remoteConfigPresenter.getGreetingMessage()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == EVENT_REQUEST_CODE) {
            data?.let { intent ->
                selectedEvent = intent.getStringExtra(EVENT_CALLBACK_KEY)
                binding.btnEvent.text = selectedEvent
            }
        } else if (requestCode == GUEST_REQUEST_CODE) {
            data?.let { intent ->
                selectedGuest = intent.getStringExtra(GUEST_CALLBACK_KEY)
                binding.btnGuest.text = selectedGuest
            }
        }
    }

    companion object {
        const val EVENT_CALLBACK_KEY = "event_name"
        const val EVENT_REQUEST_CODE = 102
        const val GUEST_REQUEST_CODE = 103
        const val GUEST_CALLBACK_KEY = "guest_name"
    }

    override fun onUpdateBaseUrlNeeded(type: String?, url: String?) {}

    override fun onUpdateTypeReceive(update: UpdateType?) {}

    override fun onGreetingReceived(message: String, isAllCaps: Boolean) {
        binding.tvWelcome.text = message
        binding.tvWelcome.isAllCaps = isAllCaps
    }

    override fun onGreetingIsLoading(message: String) {
        binding.tvWelcome.text = message
    }
}