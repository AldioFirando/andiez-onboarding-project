package com.andiez.onboarding.feature.event

import com.andiez.onboarding.base.presenter.MvpView
import com.andiez.onboarding.data.model.Event


/**
 * Created by dodydmw19 on 1/16/19.
 */

interface EventView : MvpView {

    fun onEventsLoaded(events: List<Event>?)

}