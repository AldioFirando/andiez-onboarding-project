package com.andiez.onboarding.feature.splashscreen

import android.os.Bundle
import com.andiez.onboarding.R
import com.andiez.onboarding.base.ui.BaseActivity
import com.andiez.onboarding.databinding.ActivitySplashscreenBinding
import com.andiez.onboarding.feature.home.HomeActivity
import com.andiez.onboarding.feature.login.LoginActivity

/**
 * Created by dodydmw19 on 12/19/18.
 */

class SplashScreenActivity : BaseActivity<ActivitySplashscreenBinding>(), SplashScreenView {

    private var splashScreenPresenter: SplashScreenPresenter? = null

    override fun getViewBinding(): ActivitySplashscreenBinding = ActivitySplashscreenBinding.inflate(layoutInflater)

    override fun onViewReady(savedInstanceState: Bundle?) {
        changeProgressBarColor(R.color.white, binding.progressBar)
        setupPresenter()
    }

    private fun setupPresenter() {
        splashScreenPresenter = SplashScreenPresenter()
        splashScreenPresenter?.attachView(this)
        splashScreenPresenter?.initialize()
    }

    override fun navigateToMainView() {
        goToActivity(HomeActivity::class.java,  null, clearIntent = true, isFinish = true)
    }

}