package com.andiez.onboarding.feature.guests

import androidx.core.net.toUri
import com.andiez.onboarding.R
import com.andiez.onboarding.base.ui.adapter.viewholder.BaseItemViewHolder
import com.andiez.onboarding.data.model.Guest
import com.andiez.onboarding.databinding.LayoutGridGuestBinding
import com.andiez.onboarding.helper.CommonUtils
import com.bumptech.glide.Glide

class GuestItemViewHolder(private val binding: LayoutGridGuestBinding) :
    BaseItemViewHolder<Guest>(binding) {
    private var mActionListener: OnActionListener? = null
    private var guest: Guest? = null

    fun setOnActionListener(onActionListener: OnActionListener) {
        mActionListener = onActionListener
    }

    override fun bind(data: Guest?) {
        data?.let { guest ->
            this.guest = guest
            guest.image.let {
                val uri = it.toUri().buildUpon().scheme("http").build()
                Glide.with(binding.imgProfile)
                    .load(uri)
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
                    .into(binding.imgProfile)
            }
            binding.tvBirthdate.text = CommonUtils.convertFormatDate(guest.birthdate)
            binding.tvName.text = guest.name

            binding.root.setOnClickListener { mActionListener?.onItemClick(guest) }
        }
    }

    interface OnActionListener {
        fun onItemClick(item: Guest)
    }
}