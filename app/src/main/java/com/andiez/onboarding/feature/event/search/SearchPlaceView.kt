package com.andiez.onboarding.feature.event.search

import com.andiez.onboarding.base.presenter.MvpView
import com.andiez.onboarding.data.model.Place

/**
 * Created by dodydmw19 on 1/14/19.
 */

interface SearchPlaceView : MvpView {

    fun onPlaceReceive(places: List<Place>?)

    fun onAddressReceive(address: String?)

    fun onPlaceNotFound()

    fun onFailedLoadPlaces(message: String?)

}