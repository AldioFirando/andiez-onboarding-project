package com.andiez.onboarding.feature.guests

import androidx.lifecycle.LifecycleOwner
import com.andiez.onboarding.BaseApplication
import com.andiez.onboarding.base.presenter.BasePresenter
import com.andiez.onboarding.data.local.RealmHelper
import com.andiez.onboarding.data.model.Guest
import com.andiez.onboarding.data.remote.services.APIService
import io.realm.RealmResults
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class GuestPresenter : BasePresenter<GuestView>, CoroutineScope {
    private var mvpView: GuestView? = null

    @Inject
    lateinit var apiService: APIService
    private var mRealm: RealmHelper<Guest>? = RealmHelper()
    override val coroutineContext: CoroutineContext get() = Dispatchers.IO + job
    private var job: Job = Job()

    init {
        BaseApplication.applicationComponent.inject(this)
    }

    fun getGuestCache() {
        /* from Realm Model */
        val data: RealmResults<Guest>? = mRealm?.getData(Guest::class.java, "id")

        mvpView?.onGuestCacheLoaded(data)
    }

    fun getGuestWithCoroutines() = launch(Dispatchers.Main) {
        runCatching {
            apiService.getGuest().await()
        }.onSuccess { data ->
            if (data.isNotEmpty()) {
                runCatching {
                    saveToCache(data)
                }.onSuccess {
                    mvpView?.onGuestLoaded(data)
                }.onFailure {
                    mvpView?.onFailed(it)
                }
            } else {
                if (data.isEmpty()) {
                    mvpView?.onGuestEmpty()
                } else {
                    mvpView?.onGuestLoaded(emptyList())
                }
            }
        }.onFailure {
            mvpView?.onFailed(it)
        }
    }

    private fun saveToCache(data: List<Guest>?) {
        if (data != null && data.isNotEmpty()) {
            mRealm?.saveList(data)
        }
    }

    override fun onDestroy() {}

    override fun attachView(view: GuestView) {
        mvpView = view
        if (mvpView is LifecycleOwner) {
            (mvpView as LifecycleOwner).lifecycle.addObserver(this)
        }
    }

    override fun detachView() {
        mvpView = null
    }
}