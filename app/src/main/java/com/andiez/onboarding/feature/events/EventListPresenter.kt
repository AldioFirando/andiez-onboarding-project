package com.andiez.onboarding.feature.events

import com.andiez.onboarding.base.presenter.BasePresenter
import com.andiez.onboarding.data.model.Event
import com.andiez.onboarding.helper.DataDummy

class EventListPresenter : BasePresenter<EventListView> {
    private var mvpView: EventListView? = null
    override fun onDestroy() {}

    override fun attachView(view: EventListView) {
        mvpView = view
    }

    fun getEvents() {
        val listData = mutableListOf<Event>()
        listData.addAll(DataDummy.getEventsDummy())
        mvpView?.onEventsLoaded(listData)
    }

    override fun detachView() {
        mvpView = null
    }
}