package com.andiez.onboarding.feature.guests

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import com.andiez.onboarding.R
import com.andiez.onboarding.base.ui.BaseActivity
import com.andiez.onboarding.base.ui.recyclerview.BaseRecyclerView
import com.andiez.onboarding.base.ui.recyclerview.EndlessScrollCallback
import com.andiez.onboarding.data.model.ErrorCodeHelper
import com.andiez.onboarding.data.model.Guest
import com.andiez.onboarding.databinding.ActivityGuestBinding
import com.andiez.onboarding.feature.choosebutton.ChooseButtonActivity
import com.andiez.onboarding.helper.CommonUtils
import io.realm.RealmResults
import java.text.SimpleDateFormat
import java.util.*

class GuestActivity : BaseActivity<ActivityGuestBinding>(), GuestView,
    GuestItemViewHolder.OnActionListener {
    private lateinit var guestPresenter: GuestPresenter
    private lateinit var guestAdapter: GuestAdapter

    override fun onViewReady(savedInstanceState: Bundle?) {
        setupProgressView()
        setupEmptyView()
        setupErrorView()
        setupList()
        Handler(Looper.getMainLooper()).postDelayed({
            setupPresenter()
        }, 100)
    }

    private fun setupPresenter() {
        guestPresenter = GuestPresenter()
        guestPresenter.attachView(this)
        guestPresenter.getGuestCache()
    }

    private fun setupList() {
        guestAdapter = GuestAdapter()
        binding.rvGuest.apply {
            setUpAsGrid(2)
            setAdapter(guestAdapter)
            setSwipeRefreshLoadingListener {
                loadData()
                getSwipeRefreshLayout().isRefreshing = false
            }
        }
        guestAdapter.setOnActionListener(this)
        binding.rvGuest.showShimmer()

    }

    private fun loadData() {
        guestPresenter.getGuestWithCoroutines()
    }

    private fun setData(data: List<Guest>?) {
        data?.let {
            guestAdapter.add(it)
        }
        binding.rvGuest.stopShimmer()
        binding.rvGuest.showRecycler()
    }

    private fun setupProgressView() {
        R.layout.layout_shimmer_member.apply {
            binding.rvGuest.baseShimmerBinding.viewStub.layoutResource = this
        }

        binding.rvGuest.baseShimmerBinding.viewStub.inflate()
    }

    private fun setupEmptyView() {
        binding.rvGuest.setImageEmptyView(R.drawable.empty_state)
        binding.rvGuest.setTitleEmptyView(getString(R.string.txt_empty_member))
        binding.rvGuest.setContentEmptyView(getString(R.string.txt_empty_member_content))
        binding.rvGuest.setEmptyButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
                loadData()
            }

        })
    }

    private fun setupErrorView() {
        binding.rvGuest.setImageErrorView(R.drawable.empty_state)
        binding.rvGuest.setTitleErrorView(getString(R.string.txt_error_no_internet))
        binding.rvGuest.setContentErrorView(getString(R.string.txt_error_connection))
        binding.rvGuest.setErrorButtonListener(object : BaseRecyclerView.ReloadListener {

            override fun onClick(v: View?) {
                loadData()
            }

        })
    }

    private fun showError() {
        finishLoad(binding.rvGuest)
        binding.rvGuest.showError()
    }

    private fun showEmpty() {
        finishLoad(binding.rvGuest)
        binding.rvGuest.showEmpty()
    }


    override fun getViewBinding(): ActivityGuestBinding =
        ActivityGuestBinding.inflate(layoutInflater)


    override fun onGuestCacheLoaded(guests: RealmResults<Guest>?) {
        guests.let {
            if (guests?.isNotEmpty()!!) {
                setData(guests)
            }
        }
        finishLoad(binding.rvGuest)
        loadData()
    }

    override fun onGuestLoaded(guests: List<Guest>?) {
        guests.let {
            if (guests?.isNotEmpty()!!) {
                setData(guests)
            }
        }
        finishLoad(binding.rvGuest)
    }

    override fun onGuestEmpty() {
        showEmpty()
        binding.rvGuest.setLastPage()
    }

    override fun onFailed(error: Any?) {
        error?.let { ErrorCodeHelper.getErrorMessage(this, it)?.let { msg -> showToast(msg) } }
        showError()
    }

    override fun onItemClick(item: Guest) {
        setResult(
            ChooseButtonActivity.GUEST_REQUEST_CODE,
            Intent().putExtra(ChooseButtonActivity.GUEST_CALLBACK_KEY, item.name)
        )
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val date = formatter.parse(item.birthdate)
        val calendar = Calendar.getInstance()
        calendar.time = date!!
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH) + 1
        var text = ""
        text += when {
            (day % 2 == 0 && day % 3 == 0) ->
                "iOS"
            (day % 2 == 0) ->
                "blackberry"
            (day % 3 == 0) ->
                "android"
            else -> "feature phone"
        }
        text += ", ${if (CommonUtils.isPrime(month)) "isPrime" else "not Prime"}"
        showDialogConfirmation("Guest Check", text, {
            finish()
        })
    }
}