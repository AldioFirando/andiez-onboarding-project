package com.andiez.onboarding.feature.events

import com.andiez.onboarding.base.presenter.BasePresenter

class EventActivityPresenter : BasePresenter<EventActivityView> {
    private var mvpView: EventActivityView? = null
    override fun onDestroy() {}

    override fun attachView(view: EventActivityView) {
        mvpView = view
    }

    override fun detachView() {
        mvpView = null
    }
}