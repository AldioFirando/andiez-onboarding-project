package com.andiez.onboarding.feature.event

import com.andiez.onboarding.BaseApplication
import com.andiez.onboarding.base.presenter.BasePresenter
import com.andiez.onboarding.data.model.Event
import com.andiez.onboarding.helper.DataDummy

/**
 * Created by dodydmw19 on 1/16/19.
 */

class EventPresenter : BasePresenter<EventView> {

    private var mvpView: EventView? = null

    init {
        BaseApplication.applicationComponent.inject(this)
    }

    fun getEvents() {
        val listData = mutableListOf<Event>()
        listData.addAll(DataDummy.getEventsDummy())
        mvpView?.onEventsLoaded(listData)
    }

    override fun onDestroy() {}

    override fun attachView(view: EventView) {
        mvpView = view
    }

    override fun detachView() {
        mvpView = null
    }
}